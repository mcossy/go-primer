package main

import (
	"testing"
	"reflect"
	"errors"
)

func TestDividePositive(t *testing.T) {

	expected := 100.0

	got, err := Divide(150, 1.5)

	if err != nil {
		t.Error(err)
	}
	if got != expected {
		t.Error("expected", expected, "got", got)
	}
}

func TestDivideNegative(t *testing.T) {

	expected := -2.5

	got, err := Divide(5, -2)

	if err != nil {
		t.Error(err)
	}
	if got != expected {
		t.Error("expected", expected, "got", got)
	}
}

func TestDivideError(t *testing.T) {

	expected := 0.0
	expectedError := errors.New("cannot divide by zero")

	got, err := Divide(42, 0)

	if err == nil || !reflect.DeepEqual(err, expectedError) {
		t.Error("expected", expectedError, "got", got)
	}
	if got != expected {
		t.Error("expected", expected, "got", got)
	}

}

// Using a test tablet

var testDataDivide = []struct {
	quotient      float64
	dividend      float64
	expectedValue float64
	expectedError error
}{
	{150, 1.5, 100.0, nil},
	{5, -2, -2.5, nil},
	{42, 0, 0.0, errors.New("cannot divide by zero")},
}

func TestDivide(t *testing.T) {

	for _, testData := range testDataDivide {

		t.Logf("Testing using %+v", testData)
		got, err := Divide(testData.quotient, testData.dividend)

		if !reflect.DeepEqual(err, testData.expectedError) {
			t.Error("expected", testData.expectedError, "got", err)
		}

		if got != testData.expectedValue {
			t.Error("expected", testData.expectedValue, "got", got)
		}

	}
}