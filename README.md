[TOC]

# A primer to the Go programming language

This document will roughly outline the features of the Go programming language (GoLang) and its intended audience are C#, Java and Python developers.

# About Go

https://golang.org/

Developed by Google Inc, created by [Robert Griesemer](https://research.google.com/pubs/author96.html), [Rob Pike](https://research.google.com/pubs/r.html), and [Ken Thompson](http://www.computerhistory.org/fellowawards/hall/ken-thompson/)

## Main characteristics

* Go is a compiled language
* is type safe and statically typed
* rich set of primitive data types
* no runtime needed in the target machine
* can be linked into a native executable (windows / macOS / linux, etc)
* it got a garbage collector
* supports classes (somewhat)
* supports interfaces
* code inheritance done via composition
* functions are first class citizens and are assignable as values
* lightweight multi-threading (go routines)
* uses channels for coordinating multiple go routines
* it organize modules as packages
* mature standard library
* provides native tools for Unit Testing / Test Coverage / Code Formatting , etc
* import external (git/hg/svn/etc) dependencies automatically

## Common Critic

* mandatory workspace setup and layout (`GOHOME` / `GOPATH`)
* no method/function overload
* no generics or templates
* no constructors for objects
* no `implements` keyword for interfaces, you just implement the methods
* no first-class support of classes, `struct` is used for emulating objects and classes
* no immutable objects
* public/private depends on identifier being capitalized or not
* errors are values, null pointers represent a success status
* no block-based error handling / no exceptions
* no ternary inline if operator
* code may look strange due to the lexer inserting semicolons for you
* no proper dependency management
* single implementation of map and list
* you cannot create your own native iterable objects

# Setup a Go environment and IDE

https://golang.org/doc/code.html

It is not enough to simply install the `go` package from your distributions package manager, you still need to: 

* set environment variables `GOHOME` and `GOPATH`
* create the directory structure for your local workspace

## Install Go

First off, install the go binaries using the method that better suits your operating system:

* Manual Install (Linux / MacOS / Windows): https://golang.org/doc/install
* Ubuntu Linux: https://github.com/golang/go/wiki/Ubuntu
* Arch Linux: https://wiki.archlinux.org/index.php/Go

## Create the Go workspace structure

Choose a Go root folder for the Go workspace in your user home space, this is normally `~/go` and then proceed to create the needed direcotries for it:

```
mkdir -p ~/go/{bin,pkg,src}
```

* `bin`: here will be stored the compiled/linked executable version of the projects when running `go install`
* `pkg`: here are stored your reusable compiled Go libraries
* `src`: here is where you put your projects and source code and where other dependencies will be downloaded

## Configure the environment variables

In order to be able to properly work with Go you need to define the `GOPATH` environment variable and optionally append the `bin` folder to your local `PATH` variable.

Edit your `.bashrc` file (or whatever your shell / OS uses) and append the following lines.

```
GOPATH=~/go
PATH=$PATH:$GOPATH/bin
```

## Install an IDE

https://github.com/golang/go/wiki/IDEsAndTextEditorPlugins

As of now there is not yet a perfect defacto Go IDE, after some research these seems to be the best suitable candidates I'm aware of:

* [vim-go](https://github.com/fatih/vim-go) and Co: Open source and free, the most recommended editor in several forums
* [Visual Studio Code](https://code.visualstudio.com) with [Go Plugin](https://code.visualstudio.com/docs/languages/go): Open source and free, The most stable graphic IDE in this list, very nice feature set, carefree setup.
* [LiteIDE](https://github.com/visualfc/liteide): Open source and free, excellent feature set but with a tendency to break after every new release of Go
* [GoLand](https://www.jetbrains.com/go/): Closed source and commercial, still having issues and not free of bugs, but looking very promising

# The basics of a Go project

* projects in Go are stored as a folder under `GOPATH/src`
* package can be organized as sub folders
* packages are not hierarchical, no matter how your folder structure looks like
* you can create several source files inside a package, they can share declared resources

## "Hello World"

Create a project folder named `hello` under `GOPATH/src`, create a file named `hello.go` and type the following program into it

```Go
package main

import (
	"fmt"
)

func main() {
	fmt.Println("hi there, welcome to Go!")
}
```

and run it using

```
go run hello.go
```

or create an executable in the local project folder using

```
go build
```

Congratulations, you first Go executable got compiled and linked!

## the fundamentals

* The package `main` is the root package and is mandatory for every project intended to be an **executable**
* the function `func main() { }` is the entry point of your executable program
* build a project and generate an executable in the project folder: `go build`
* run all tests in project `go test ./...`
* install the executable into `GOPATH/bin` by using `go install`

# Language basics

## Null values

```Go
nil
```

* `nil` is the zero value for pointers, interfaces, channels, slices, maps and function types, and it represents an uninitialized state.
* `nil` also represents the absence of an error for the `error` data type
* `nil` cannot be assigned to primitive types, including strings. It is possible to `nil` a pointer to a primitive data type, though.

## Primitive data types

### Logical

```Go
bool
```

### Strings / sequence of Unicode characters

```Go
string
```

### Integer

Are organized into signed integers:

```Go
int  int8  int16  int32  int64
```

and unsigned integers

```Go
uint uint8 uint16 uint32 uint64 uintptr
```

It is important to notice that the size `int`, `uint` and `uintptr` are system dependent.

### Bytes and Unicode characters

A single byte and at the same time an alias for `uint8`

```Go
byte
```

A single Unicode code point or character and at the same time for `int32`

```Go
rune
```

### IEEE-754 floating point

```Go
float32 float64
```

### Complex numbers

```Go
complex64 complex128
```

## Zero values

Zero values are the ones assigned to data types if not explicitly initialized

* `0`  : for numeric types,
* `false` : for the `bool` type,
* `""` : (the empty string) for strings,
* `nil`: for pointers, interfaces, channels, slices, maps and function types

## Variables

Simple declaration without value

```Go
var i int
var c, python, golang bool
```

Simple declaration with value

```Go
var i int = 42
var c, python, golang bool = true, false, true
```

Declaration with inferred data types, this allows also mixing the data types

```Go
var score, name, running = 42000, "player1", true
```

Using a `var` declaration block

```Go
var (
	ToBe   bool       = false
	MaxInt uint64     = 1<<64 - 1
	z      complex128 = cmplx.Sqrt(-5 + 12i)
)
```

## Inference with :=

You can only use `:=` within a function, it allows creating and initializing variables using the right side of the declaration and there is no need of the `var` keyword

```Go
i := 42           // int
f := 3.142        // float64
g := 0.867 + 0.5i // complex128
```

Multiple inline inferred values:

```Go
c, python, golang := true, false, "yes!"
```

Inferring the type of the other variables

```Go
var i int
j := i // j is an int
```

Inferring the values of a multi value function

```Go
total, err := sum(40, 2)
```

## Casting

The compiler will normally prevent you from writing impossible casts in your code.

Please take into account that Go also got type checking, we are going to visit this topic later.

```Go
var i int = 42
var f float64 = float64(i)
var u uint = uint(f)

i := 42
f := float64(i)
u := uint(f)
```

## Constants

Only primitive types can be used as constants, a simple declaration:

```Go
const Pi = 3.141592653
```

Referencing other constants

```Go
const (
	// Create a huge number by shifting a 1 bit left 100 places.
	Big = 1 << 100
	// Shift it right again 99 places, so we end up with 1<<1, or 2.
	Small = Big >> 99
)
```

Using `iota` within a `const` block, it start counting from 0 and increments by one for every declaration. It is also worth noticing that following declarations without a value just copy the declaration, **not the value**, of the previous one.

```Go
const (
  zero = iota * 1000
  thousand
  twothousand
)
```

## Functions

single return value functions, parameter types declared separately

```Go
func add(x int, y int) int {
	return x + y
}
```

multiple return value functions, parameter types declared in a single block

```Go
func swap(x, y string) (string, string) {
	return y, x
}
```

using named return parameters. This is quite peculiar because the return variables are initialized with its "zero" values and there is no need to reference them when using `return`

```Go
func split(sum int) (first, second int) {
	first = sum * 4 / 9
	second = sum - x
	return
}
```

# Packages and visibility of members

## The `main` package

This package is the root package where Go looks for the `func main() {}` entry method. It is only needed for executable programs, and can be ignored for projects intended to be used as libraries.

```Go
package main

func main() {
	// I do nothing
}
```

## Importing local/build-in packages

You can import build in packages using its absolute paths to `(GOPATH|GOROOT)/pkg/(platform)`. Please notice that it is not recommended to import packages using relative paths.

A reference for the currently available build-in packages: https://golang.org/pkg/

```Go
package main

import (
	"fmt"
	"math/rand"
)

func main() {
	fmt.Println("A random number:", rand.Intn(100))
}
```

## Importing remote GIT/HG packages

If an imported package name is prefixed with a domain name, then Go tries to perform a GIT/HG checkout from that location

```Go
package main

import (
  "github.com/gorilla/mux"
  "golang.org/x/net/html"
)

func main() {
	// nothing
}
```

in order to fetch the packages you must execute a `got get` command according to the documentation provided by the package maintainer. Please notice that this looks like dependency management, but it is not, by far.

```
go get "github.com/gorilla/mux"
```

Normally you can try to import all remote dependencies by issuing the following command:

```
go get ./...
```

## Member visibility

GO lacks of keywords like `export`, `public` or `private`.

In Go, a name is exported or made public if it begins with a capital letter.

When importing a package, you can refer only to its exported/public names. Any "unexported" names are not accessible from outside the package. 

```Go
package cooltools

// This constant will be accessible to anyone importing the module "cooltools"
const Pi = 3.141592653

// this constant is private and can be only used by its package
const somewhatPi = 3

// This method will be accessible to anyone importing the module "cooltools"
func Sum(a, b int) int {
  return sumHelper(a, b)
}

// this method is private and can be only used by its package
func sumHelper(a, b int) int {
  return a + b;
}
```

## the init() function

Each source file can define its own `func init() {...}` function which is executed:

* after all package member variables were initialized
* before any other referencing/importing packages can access the current package

# Error handling

In go there are no exceptions, it is not possible to declare an atomic block of operations (`try`) and react later to any problems that may occur in afterwards (`catch` / `except`). If you need a `finally` / `else` block then you must use a `defer` function. More on `defer` functions later.

## Errors are values

In go you must represent an error using a value. Any value you want, of any data type type you need. But normally you will use the officially proposed `error` data type which implement the `Error` interface.

As per convention:

* errors are normally returned as the last return parameter in a function.
* Non `nil` values are considered an error

The basic procedure is to call a function and in the next line to check if the returned error value is `nil`

```Go
package main

import (
  "errors"
  "fmt"
)

func main() {

  // will print "42"
  result1, err := Divide(84, 2)
  if err != nil {
    fmt.Println(err)
  }
  fmt.Println(result1)

  // will print the error and then "0"
  result2, err := Divide(84, 0)
  if err != nil {
    fmt.Println(err)
  }
  fmt.Println(result2)

}

func Divide(a, b float64) (float64, error) {
  if b == .0 {
    return .0, errors.New("cannot divide by zero")
  }
  return a / b, nil
}
```

## Panic for non recoverable errors

If your code found a non recoverable or fatal condition that renders any further progress of the logic as impossible, then you may consider setting your program into panic mode. 

It is possible to recover a panic

If a program exits in panic mode it will also produce a non zero exit code for the OS and it will print the stack trace to the stderr stream. You can achieve this in many ways:

Using the `panic` keyword directly

```Go
if err != nil {
	panic(err)
}
```

Using a `log`ger, which will log the problem and set the program into panic mode:

```Go
import (
	"log"
)

if err != nil {
	log.Panic(err) // enter into panic
}
```

Using the `log`ger but producing the code to immediately stop execution

```Go
import (
	"log"
)

if err != nil {
	log.Fatal(err) // exit code 1
}
```

## Example: Using the AWS SDK

AWS provides an official version the the AWS SDK

* https://aws.amazon.com/sdk-for-go/
* https://docs.aws.amazon.com/sdk-for-go/v1/developer-guide/configuring-sdk.html
* http://docs.aws.amazon.com/sdk-for-go/api/

This example shows the basics for using the official GO AWS SDK

```Go
package main

import (
	"fmt"
	"log"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/cloudwatch"
)

func main() {

	// create the credentials
	awsConfig := &aws.Config{
		Region:      aws.String("us-east-1"),
 		Credentials: credentials.NewStaticCredentials("ACCESSKEY", "SECRETKEY", ""),
	}

	// create an AWS session or fail
	awsSession, err := session.NewSession(awsConfig)
	if err != nil {
		log.Fatal(err)
	}

	// create a cloudwatch client
	cwclient := cloudwatch.New(awsSession)

	// create a request object for a specific alarm
	describeAlarmsInput := &cloudwatch.DescribeAlarmsInput{
		AlarmNames: []*string{aws.String("gl-us-vir-1-graylog-ELB-NoServers")},
	}

	// execute the query and checkf for errors
	alarms, err := cwclient.DescribeAlarms(describeAlarmsInput)
	if err != nil {
		log.Fatal(err)
	}

	// display the results
	fmt.Println(alarms)
}
```

# flow control

## if / else / else if

Note that you don’t need parentheses around conditions in Go, but that the braces are required and as in most languages, the `else` part is optional. .

```Go
if x > 0 {
	return "is positive"
}
```

using `if` / `else` and `else if`

```Go
if x > 0 {
	fmt.Println("is positive")
} else if x < 0 {
	fmt.Println("is negative")
} else {
	fmt.Println("is zero")
}
```

Using an initialization block and a conditional block. Notice that the variables declared in the first block are accessible within the `if` local scope. 

```Go
if v := math.Pow(x, n); v < lim {
	return v
}
```

That also means that the variables can be also accessed in the `else` block

```Go
if v := math.Pow(x, n); v < lim {
	return v
} else {
	fmt.Printf("%g >= %g\n", v, lim)
}
```

## for

The for loop has three components separated by semicolons and there are no parentheses surrounding them:

* the init statement: executed before the first iteration
* the condition expression: evaluated before every iteration
* the post statement: executed at the end of every iteration

```Go
for i := 0; i < 10; i++ {
	sum += i
}
```

The init and post statement are optional:

```Go
sum := 1
for ; sum < 1000; {
	fmt.Println(sum)
	sum += sum
}
```

C++/C#/Java `while` is spelled `for` in Go. Just drop the semicolons:

```Go
sum := 1
for sum < 1000 {
	sum += sum
}
```

And this is the classic infinite loop:

```Go
for {
}
```

## switch

It behaves like in other languages with the exception that a case a case body breaks automatically, unless it ends with a `fallthrough` statement. 

Switch cases evaluate cases from top to bottom, stopping when a case succeeds

A more detailed documentation can be found here: https://github.com/golang/go/wiki/Switch


This example uses an initialization block:

```Go
switch os := runtime.GOOS; os {
case "darwin":
	fmt.Println("Running on Mac OS X.")
case "linux":
	fmt.Println("Running the penguin.")
default:
	fmt.Printf("Running something else, like %s.", os)
}
```

does not call `f()` if `i==0` :

```Go
switch i {
case 0:
case f():
}
```

Switch without a condition is the same as `switch true`.  This construct can be a clean way to write long `if` / `else if` chains. 

```Go
switch {
case t.Hour() < 12:
	fmt.Println("Good morning!")
case t.Hour() < 17:
	fmt.Println("Good afternoon.")
default:
	fmt.Println("Good evening.")
}
```

We will see **type switches** later together with interfaces, but for the sake of completeness we will shortly mention them here:

A type switch needs a variable or pointer to an empty interface type `interface{}` and allows performing an action based on the type of the variable:

```Go
// variable i must be of type interface{}
switch v := i.(type) {
case string:
    fmt.Println("hello " +  v + "! Nice to meet you.")
case int:
    fmt.Println(v * 2)
default:
    fmt.Printf("I don't know about type %T!\n", v)
}
```

## defer

A `defer` statement defers the execution of a function until the surrounding function returns.

The deferred call's arguments are evaluated immediately, but the function call is not executed until the surrounding function returns. 

The most common use for defer is to close and release resources allocated by a function, you may think of this as a `finally` clause in C#/Java

```Go
func main() {
	defer fmt.Println("world")
	fmt.Println("hello")
}
```

Deferred function calls are pushed onto a stack. When a function returns, its deferred calls are executed in last-in-first-out order. 

```Go
func main() {
	fmt.Println("counting")

	for i := 0; i < 5; i++ {
		fmt.Println("in loop", i)
		defer fmt.Println("deferred", i)
	}

	fmt.Println("end of function")
}
```

Example on releasing a mutex lock, no matter how the function call ends

```Go
func transaction() {
	mu.Lock()
	defer mu.Unlock()
	// code that may fail goes here
}
```

## recover from panic

you can use `recover` withing a `defer`red function call for capturing and interrupting an ongoing `panic`

Read https://blog.golang.org/defer-panic-and-recover for more details

It is important to notice that it is not necessary to `recover` the `panic` in the same function where panic occurs. The `defer`red function can be anywhere in the active function call stack.

```Go
func main() {

	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered", r)
		}
	}()

	fmt.Println("doing stuff...")
	panic("all is lost")
}
```

# Structures and values

In Go everything is passed by value.

This means that when you pass a primitive variable or a structure to a function, the receiving method get a perfect copy of it. And any changes applied to it will not reflect on the original variable or structure.

The only way to avoid this is by using and passing pointers between functions.

## Pointers

Pointers are special integer values containing a memory address where the declared data type is stored.

### Declaration

You can declare a pointer to any data type by prefixing an `*` before it.

```Go
var p *int
```

### Accessing

* `&` operator to find the address of a variable, it returns a pointer.
* `*` is also used to “dereference” pointer variables. Dereferencing a pointer gives us access to the value the pointer points to.

```Go
var p *int      // create an int pointer
var i int = 42  // create a plain int variable

p = &i          // point to i
*p = 21         // set i through the pointer p

fmt.Println(*p) // read i through the pointer
fmt.Println(p)  // read the address of i 
fmt.Println(&p) // read the address of the pointer to i
```

## `&` vs `new`

The `new` keyword allows creating a new instance of a data type and return its pointer

The `&` operator returns the pointer of an existing, already created value

Most of the time you will be using `&` instead of `new`, specially when dealing with `struct` types (see next topic).

At the moment one of the few uses of the `new` operator is to create a pointer to a primitive data type.

```Go
p := new(int)   // creates a new pointer to an integer 
*p = 4          // assigns the integer by dereferencing the pointer 
fmt.Println(p)  // print the address of the pointer
fmt.Println(*p) // print the value behind the pointer
```

## Structures

A `struct` is an organized set of member variables of arbitrary data type. The order of the elements is relevant.

A `struct` is also the is the basic element used to create **classes and objects**, more on this in the next chapter.

```Go
type Vertex struct {
	x int
	y int
	label string
}

func main() {

	fmt.Println(Vertex{1, 2, "test"})

	v := Vertex{1, 2, "blub"}
	v.x = 4
	fmt.Println(v)

}
```

in the previous example `v` is a value variable and when passed to `fmt.Println(v)` Go will create a copy of it in order to execute the `Println` method.

You can create a pointer to a structure during declaration by using `&`

```Go
v := &Vertex{1, 2, "test"}
```

The variable `v` is now a pointer to a Vertex structure.

Notice that you can use the `new` operator for creating a pointer to a structure, but by doing this you cannot initialize the structure during creation:

```Go
v := new(Vertex)
v.x = 1
v.y = 2
v.label = "test"
``` 

Pointers to Structs need no `*` to access to the members

```Go
v := Vertex{1, 2, "test"}
p := &v
p.X = 123
```

All members of a `struct` are automatically initialized to its zero values, you can use the name of the members to initialize only some of them. Using names during the construction of a `struct` allows you to change the order of the passed values and also to omit others.

```Go
type Vertex struct {
	X, Y int
}

var (
	v1 = Vertex{1, 2}  // has type Vertex
	v2 = Vertex{X: 1}  // Y:0 is implicit
	v3 = Vertex{}      // X:0 and Y:0
	p  = &Vertex{Y:2, X:1} // has type *Vertex
)
```

## Structure composition

It is possible to compose or embed a structure into another, by doing this methods and members of of different structure can be inherited.

This is the only available way of code inheritance in Go, and it got some shortcomings.

### Composing

Composing is quite easy, you write the plain name(s) of the data type structure(s) to be embedded into the definition of the other one:

```Go
type A struct {
	MemberA int
}

type B struct {
	A
	MemberB float64
}
```

In this example the type `B` is inheriting all definitions from type `A`

But beware, this does no mean that `B` is an `A`! You cannot pass or assign a `B` structure to a variable of type `A`

### Initializing

Initializing a composed structure can be tricky, you **CAN NOT** do this:

```Go
// this will NOT work!
valueB := &B{
    MemberA: 42,    // <- no
    MemberB: 3.1415,
}
```
but instead you have to create the sub structure accordingly:

```Go
// ok
valueB := &B{
    A: A{MemberA: 42},
    MemberB: 3.1415,
}
```

### Accessing members

Here things look better, you can access the substructure explicitly:

```Go
fmt.Println(valueB.A.MemberA)
fmt.Println(valueB.MemberB)
```

or transparently with some help of the the compiler:

```Go
fmt.Println(valueB.MemberA)
fmt.Println(valueB.MemberB)
```

### Soft of casting

As stated before, although `B` is inheriting or being composed with `A`, this does not mean that you can assign `B` to a variable of type `A`

You need to access directly the inherited data type for achieving this:

```Go
var valueA A = valueB   // nope
var valueA A = valueB.A // yes
```

## Custom type

You use `type` not only for defining strucst but also for defining your own data types. 

```Go
type MyFloat float64
f := MyFloat(3.14)
```

This seems at first of no real use. Its usefulness will become evident in the next chapter when we learn to attach methods to `type`s and `struct`s

## Arrays

Arrays in go have a fixed length and can store only the data type specified during declaration.

Any declaration looking like `[size]type` is an array, if the size is missing then you are looking at a slice, more on slices soon.

```Go
func main() {

	// declare a "zeroed" array
	var a [2]string

	// access elements
	a[0] = "Hello"
	a[1] = "World"
	fmt.Println(a[0], a[1])
	fmt.Println(a)

	// initialize an array during declaration
	primes := [6]int{2, 3, 5, 7, 11, 13}
	fmt.Println(primes)

}
```

by using `...` in the declaration you can have the compiler count the array elements for you

```Go
primes := [...]int{2, 3, 5, 7, 11, 13}
```

you can read the size of an array by using the build in function `len`

```Go
len(primes) // will return 6
```

Most programs do not use arrays directly, but use slices instead.

## Slices

Recommended read: https://blog.golang.org/go-slices-usage-and-internals

* A slice does not store any data, it just describes a section of an underlying array.
* Changing the elements of a slice modifies the corresponding elements of its underlying array. 
* The length and capacity of a slice `s` can be obtained using the expressions `len(s)` and `cap(s)`. 
* you can create a slice from an array by using the interval syntax `arr[from:to]`

```Go
func main() {
	// create an array
	primes := [6]int{2, 3, 5, 7, 11, 13}

	// get a slice of the array
	var s []int = primes[1:4] // [3 5 7]
	fmt.Println(s)
}
```

The arguments of the interval syntax are optional and default to the min and max values of the array or slice length

```Go
// the array
primes := [6]int{2, 3, 5, 7, 11, 13}

var s1 []int = primes[1:4] // [3 5 7]
var s2 []int = primes[:4]  // [2 3 5 7]
var s1 []int = primes[2:]  // [5 7 11 13]
var s1 []int = primes[:]   // [2 3 5 7 11 13]

}
```

It is possible to create and initialize a slice using a literal, please notice the missing size argument compared to the array declaration 

```Go
primes := []int{2, 3, 5, 7, 11, 13}
```

If you just declare slice like `s := []int` it will be empty and accessing any index of it will cause an error. You need helper build in functions in order to circumvent this.

### make

The `make` function allocates a zeroed array and returns a slice that refers to that array:

```Go
a := make([]int, 5)    // len(a)=5

b := make([]int, 0, 5) // len(b)=0, cap(b)=5
b = b[:cap(b)]         // len(b)=5, cap(b)=5
b = b[1:]              // len(b)=4, cap(b)=4
```

### append

This function will write elements at the end of the slice, the first parameter of `append` is a slice of type T, and the rest are T values to append to the slice. 

```Go
s = append(s, 2, 3, 4)
```

It is important to reassign the slice to the same variable, because the function may modify and reallocate memory in order to make the elements fit into the underlying array.

### copy

The copy built-in function copies elements from a source slice into a destination slice.

Notice that the order of the parameters is counter intuitive.

```Go
source := []int{1,2,3,4,5}

shortTarget := make([]int, 3)  // [0 0 0] 
longTarget := make([]int, 10)  // [0 0 0 0 0 0 0 0 0 0] 

copy(shortTarget, source)      // [1 2 3]
copy(longTarget, source)       // [1 2 3 4 5 0 0 0 0 0]
```

As an special case, you can copy a `string` into a `byte` slice

```Go
target := make([]byte, 5)
copy(target, "hello")          // [104 101 108 108 111]
```

### Unwinding slices in varargs parameters

You can declare a slice as a vararg parameter in a function by prepending a `...` before the type

```Go
func myFunc( values ...int) int {
    // some logic here	
}
```

and pass a slice to it either literally:

```Go
myFunc(3, 5, 6, 7)
```

or by appending a `...` after the a slice variable:

```Go
values := []int{3, 5, 6, 7}
myFunc(values...)
```

## range

A range clause provides a way to iterate over an array, slice, string, map, or channel and is always used together with a `for` clause

depending on the provided data type `range` will return one or two values, as follows

| type | 1st value | 2nd value |
| --- | --- | --- |
| array `[n]E` | index `i int` | `a[i] E` |
| slice `[n]E` | index `i int` | `a[i] E` |
| string | index `i int` | value `rune` |
| map[K]V |	key `k K` | 	value `m[k] V` |
| channel `c chan E` | element `e E` | none |

```Go
var pow = []int{1, 2, 4, 8, 16, 32, 64, 128}

func main() {
	for i, v := range pow {
		fmt.Printf("2**%d = %d\n", i, v)
	}
}
```

You can skip the index or value by assigning to `_`.

```Go
for _, value := range pow {
	fmt.Printf("%d\n", value)
}
```

If you only want the index, drop the  value variable entirely. 

```Go
for i := range pow {
	pow[i] = 1 << uint(i) // == 2**i
}

```

## map

Maps are dictionaries or associative arrays, they create an association between a key and a value.

You can create a empty map variable this way, but you will not be able to access its data. This kind of declaration is only useful for getting a reference from another map.

```Go
var m map[string]Vertex
```

In order to create an usable `map` you must ise the `make` clause:

```Go
m := make(map[string]Vertex)
```

There is a `delete` keyword which can only be used with maps and is intended to remove a key from the map in question.

Basic map operations:

```
m[key] = elem       // insert or update an element

elem = m[key]       // retrieve an element

delete(m, key)      // remove the element associated to the key

elem, ok = m[key]   // retrieve an element and check if the key exists

_, ok = m[key]      // only check if it the key exists
```

Example creating a map of Vertex structures

```Go
package main

import "fmt"

type Vertex struct {
	Lat, Long float64
}

func main() {
	m := make(map[string]Vertex)
	m["Bell Labs"] = Vertex{
		40.68433, -74.39967,
	}
	fmt.Println(m["Bell Labs"])
}
```

Initializing a map using literals:

```Go
var m = map[string]Vertex{
	"Bell Labs": Vertex{40.68433, -74.39967},
	"Google": Vertex{37.42202, -122.08408},
}
```

If the top-level type is just a type name, you can omit it from the elements of the literal. 

```Go
var m = map[string]Vertex{
	"Bell Labs": {40.68433, -74.39967},
	"Google":    {37.42202, -122.08408},
}
```

## Function as values

Functions are values and can be passed to or returned from other functions. They can be assigned to variables just like other data types. Function values may be used as function arguments and return values. 

### Anonymous functions

Anonymous functions are functions assigned to local variables that can be used withing the scope they were defined. This is useful if you want to avoid to declare and name a function in your code which may only be used by a very restricted part of your code.

```Go
func main() {

	fsum := func(a, b int) int {
		return a + b
	}

	fmt.Println(fsum(1, 1))
	fmt.Println(fsum(40, 2))

}
```

### Function as parameter values

You can pass a function as a parameter to another function and then invoke it from there. Pay attention to the declaration of the `use` function

```Go
const pi = 3.1415
const phi = 1.618

// will pass pi and phi to the provided functions
func use(fn func(a, b float64) float64) float64 {
	return fn(pi, phi)
}

func main() {

	// declare some anonymous functions
	sum := func(a, b float64) float64 {
		return a + b
	}
	diff := func(a, b float64) float64 {
		return a - b
	}
	mult := func(a, b float64) float64 {
		return a * b
	}
	quot := func(a, b float64) float64 {
		return a / b
	}

	// pass the anonymous functions as values to "use"
	fmt.Println(use(sum))
	fmt.Println(use(diff))
	fmt.Println(use(mult))
	fmt.Println(use(quot))

}
```

### Function closures

You can also return a function as a value from another function. The interesting part in this is that the local variables and parameters of the surrounding function are bound and available to the returned function **instance**.

Take a look into the `sequence` method and how the variable `sum` and the parameter `step` are kept private and bound to the returned functions.

```Go
func sequence(step int) func() int {
	sum := 0
	return func() int {
		sum += step
		return sum
	}
}

func main() {
	counter := sequence(1)
	negative := sequence(-9)
	for i := 0; i < 10; i++ {
		fmt.Println(counter(), negative())
	}
}
```

# Classes and methods

`type struct` is used for implementing classes.

Go supports classes and object instances and is one of the most used features of the language. On the other hand Go "classes" are not as powerful as in order languages, some of the missing OOP features are

* no constructors methods
* no abstract functions / classes
* no immutable members
* no generics or templates

## Constructors

In go there are no constructors, the nearest language feature are the literal initializers for `struct`s, consider the following example:

```Go
type Matrix struct {
	cols     int
	rows     int
	elements []float64
}

func main() {
	m := &Matrix{
		cols:     10,
		rows:     5,
		elements: make([]float64, 10*5),
	}
}
```
This works most of the time, but in some cases you depend on the user for a proper object initialization, like the `elements` member in the previous example.

An alternative is to make the structure private (lower case name) and provide a public function which acts as constructor.

```Go
type matrix struct {
	cols     int
	rows     int
	elements []float64
}

func NewMatrix(rows, cols int) *matrix {
	m := &matrix{}
	m.rows = rows
	m.cols = cols
	m.elements = make([]float64, rows*cols)
	return m
}
```

## Methods

in Go a class method is not declared within the `struct` it belongs to, but as a separated function entity.

A class method is nothing else than a normal `func`tion with additional prefixed receiver information. The receiver is the name of the `struct` or `type` you want to attach the method to.

```Go
func (m matrix) size() int {
	return m.cols * m.rows
}
```

`(m matrix)` is the receiver, and the variable `m` can be used to access the object instance data.

Now you can use the method with any `matrix` instance:

```Go
func main() {
	m := NewMatrix(3, 4)
	size := m.size()
}
```

## By value methods

By value methods are declared with a flat, non pointer receiver, like this one `(m matrix)`

It is important to understand that a by-value method **will get a copy of the structure data** to operate on. This should be used when you want to prevent the implementation to modify the object/struct on which the call is being performed.

The following example will get a copy of the `matrix` struct and calculate its size:

```Go
func (m matrix) size() int {
	return m.cols * m.rows
}
```

The following example shows a classic pitfall, it modifies the size of the matrix, but the modification is done on the passed copy, the caller structure will remain unmodified:

```Go
func (m matrix) setCols(newColValue int) {
	m.cols = newColValue
}
```

therefore this code will **not do** what you expect:

```Go
m := NewMatrix(3, 4)
fmt.Println(m.cols) // prints "4"
m.setCols(10)
fmt.Println(m.cols) // prints "4" again, not "10""
```

## By reference methods

By reference methods are declared with pointer receiver, like this one `(m *matrix)`.

The method will get a reference pointer to the struct on which the call was done and the logic will be able to modify the source struct. This kind of methods are the default on C# and Java.

Here the modified version of `setCols` using a pointer receiver

```Go
func (m *matrix) setCols(newColValue int) {
	m.cols = newColValue
}
```

therefore this code **will do** what you expect:

```Go
m := NewMatrix(3, 4)
fmt.Println(m.cols) // prints "4"
m.setCols(10)
fmt.Println(m.cols) // prints "10"
```

## Add methods to primitive types

In order to add methods to primitive types you must create your own type based on the primitive you want to extend and then append the methods using receivers

```Go
type MyFloat float64

func (f MyFloat) Abs() float64 {
	if f < 0 {
		return float64(-f)
	}
	return float64(f)
}

func main() {
	var i MyFloat = -3.14
	fmt.Println(i.Abs())
}
```

# interfaces

An interface type is defined as a set of method signatures.

A type implements an interface by implementing its methods. There is no explicit declaration of intent, no `implements` keyword.

## by-value / by-reference implementations

The implementing methods can use by-pointers or by-value receivers.

```Go
type Abser interface {
	Abs() float64
}

// by value implementation
type MyFloat float64

func (f MyFloat) Abs() float64 {
	if f < 0 {
		return float64(-f)
	}
	return float64(f)
}

// by reference implementation
type Vertex struct {
	X, Y float64
}

func (v *Vertex) Abs() float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}


func main() {
	// using the interface
	var a Abser

	f := MyFloat(-math.Sqrt2)
	v := Vertex{3, 4}

	a = f  // a MyFloat implements Abser
	a = &v // a *Vertex implements Abser
	
	fmt.Println(a.Abs())
}
```

## interface{}

The interface type that specifies zero methods is known as the empty interface and it can be compared to `Object` in C#/Java  (this is not 100% correct)

Empty interfaces are used by code that handles values of unknown type. For example, `fmt.Print` takes any number of arguments of type `interface{}`.

```Go
func main() {
	var i interface{}
	describe(i)

	i = 42
	describe(i)

	i = "hello"
	describe(i)
}

func describe(i interface{}) {
	fmt.Printf("(%v, %T)\n", i, i)
}
```

will output

```
(<nil>, <nil>)
(42, int)
(hello, string)
```

## Type assertion

A type assertion provides access to an interface value's underlying concrete typed value.

To test whether an `interface{}` value holds a specific type, a type assertion can return two values: 

* the underlying typed value
* and a boolean value that reports whether the assertion succeeded or not

```Go
var i interface{} = "hello"

s := i.(string)
fmt.Println(s) // hello

s, ok := i.(string)
fmt.Println(s, ok) // hello , true

f, ok := i.(float64)
fmt.Println(f, ok) // 0 , false

f = i.(float64) // panic, no ok variable specified
fmt.Println(f)
```

## Type switches

A type switch is a construct that permits several type assertions in sequence. The declaration in a type switch has the same syntax as a type assertion `i.(T)`, but the specific type T is replaced with the keyword `type`.

A type switch needs a variable or pointer to an empty interface type `interface{}` and allows performing an action based on the type of the variable

Within every single `case` you can use the **typed value** from the `interface{}`

```Go
func do(i interface{}) {
	switch v := i.(type) {
	case int:
		fmt.Printf("Twice %v is %v\n", v, v*2)
	case string:
		fmt.Printf("%q is %v bytes long\n", v, len(v))
	default:
		fmt.Printf("I don't know about type %T!\n", v)
	}
}

func main() {
	do(21)
	do("hello")
	do(true)
}
```

will output

```
Twice 21 is 42
"hello" is 5 bytes long
I don't know about type bool!
```

## Some notable interfaces

### fmt.Stringer interface

Equivalent to Java's `toString()`, Notice that the implementation must be done using a by-value implementation.

```Go
type Stringer interface {
    String() string
}
```

### error interface

Used as the default type for handling errors 

```Go
type error interface {
    Error() string
}
```

Creating your custom errors:

```Go
type MyError struct {
	When time.Time
	What string
}

// interface implemented
func (e *MyError) Error() string {
	return fmt.Sprintf("at %v, %s", e.When, e.What)
}

// method declares an error interface as return value, it returns our own implementation
func run() error {
	return &MyError{
		time.Now(),
		"it didn't work",
	}
}

func main() {
	if err := run(); err != nil {
		fmt.Println(err)
	}
}
```

# Concurrency

## GoRoutines

It is possible to dispatch the execution of a function into a separated thread, or "go routine".

This is done by prepending the `go` keyword before the function call.

It is important to clarify that go routines are daemons which will **not** prevent the program to exit if they are still executing when the main thread running `func main()` terminates.

```Go
go f(x, y, z)
```

Note: The **evaluation** of `f`, `x`, `y`, and `z` happens in the current goroutine and the **execution** of `f` happens in the new goroutine.

Example:

```Go
func say(s string) {
	for i := 0; i < 5; i++ {
		time.Sleep(100 * time.Millisecond)
		fmt.Println(s)
	}
}

func main() {
	go say("world")
	say("hello")
}
```
## Channels

Channels are blocking queues through which you can send and receive values with the channel operator `<-`.

By default channels have a capacity of zero elements, write and read operations will **block** until the other side is ready. This allows goroutines to synchronize without explicit locks or condition variables.

creating a channel of `int`

```Go
channel := make(chan int)
```

writing/sending to a channel

```
channel <- 42
```

reading/receiving from a channel

```Go
theAnswer := <-channel
```

Example, adding to arrays in parallel:

```Go
func sum(toSum []int, channel chan int) {
	sum := 0
	for _, v := range toSum {
		sum += v
	}
	channel <- sum
}

func main() {

	channel := make(chan int)

	someNumbers := []int{7, 2, 8, -1, 4, 14}
	moreNumbers := []int{42, 12, 23, 5, 6, 7, 90, 2, -5}

	go sum(someNumbers, channel)
	go sum(moreNumbers, channel)

	x, y := <-channel, <-channel // receive from channel

	fmt.Println(x, y)
}
```
## Buffered channels

It is possible to define a channel with capacity greater than 0, in other words, a channel with a buffer.
 
Write operations to a buffered channel will only block when the buffer is full. 

Read operations from a channel wil block when the buffer is empty.

Creating `byte` a channel with a buffer of one kilobyte:   

```Go
ch := make(chan byte, 1024)
```

## Closing channels

A sender can close a channel to indicate that no more values will be sent.

Receivers can test whether a channel has been closed by assigning a second parameter to the receive expression

* Only the sender should close a channel, never the receiver
* Sending on a closed channel will cause a panic
* Channels are not like files; you don't need to close them

Closing a channel

```Go
close(ch)
```

Reading from a channel that may be closed

```Go
v, ok := <-ch
```

The boolean variable `ok` will be `false` if the channel was closed 


## Using `range` and `close`

You can read from a channel using a `for` / `range` loop.

The loop will break when the sender closes the channel.

```Go
channel := make(chan rune, 10)

channel <- 'P'
channel <- 'I'
channel <- '='
channel <- '3'
channel <- '.'
channel <- '1'
channel <- '4'
close(channel)

for char := range channel {
    fmt.Print(string(char)) // will print "PI=3.14"
}
```

## Channel directions

When using channels as function parameters, you can specify if a channel is meant to only send or receive values.

```Go
func transfer(reader <-chan string, writer chan<- string) {
    msg := <-reader
    writer <- msg
}
```

## Select with multiple channels

The `select` statement lets a goroutine wait on multiple communication operations.

A `select` **blocks** until one of its cases can run, then it executes that case. It chooses one at random if multiple are ready.

```Go
func ship(chanX chan int, chanY chan int, chanPwr chan bool) {

	x, y, power := 0, 0, false

	for {
		select {
		case deltaX := <-chanX:
			x += deltaX
			fmt.Println(x, y)
		case deltaY := <-chanY:
			y += deltaY
			fmt.Println(x, y)
		case power = <-chanPwr:
			if !power {
				fmt.Println("power OFF")
				return
			}
			fmt.Println("power ON")
		}
	}

}

func main() {

	moveX := make(chan int)
	moveY := make(chan int)
	power := make(chan bool)

	go ship(moveX, moveY, power)

	power <- true
	moveX <- 1
	moveX <- 1
	moveX <- 2
	moveY <- 1
	moveY <- 3
	moveX <- -2
	moveY <- 5
	power <- false

}
```

A `select` will not block if there is a `default` case. The default case will be executed if there is no activity on all other channels.

## sync.Mutex

This is the default mutual exclusion handler in Go.

Only one thread can acquire the lock, subsequent threads will wait until the lock us released.

Best used together with `defer`

```Go
import (
	"sync"
)

var mux = &sync.Mutex{}

func transaction() {
	mux.Lock()
	defer mux.Unlock()
	// run code without race conditions
}
```


## sync.WaitGroup

A `sync.WaitGroup` can be used to wait for a collection of GoRoutines to finish.

The controlling thread calls the `Add(int)` method to declare every additional set of worker threads being dispatched.

The worker thread call the `Done` method when its task is finished. This decreases the counter by one.

A call to `Wait` will block until all GoRoutines have finished.

```Go
import (
	"sync"
	"time"
	"fmt"
)

func complexCalculation(id int, duration time.Duration, wg *sync.WaitGroup) {
	defer wg.Done()
	time.Sleep(duration * time.Millisecond)
	fmt.Println("goroutine", id, "done")
}

func main() {

	waitGroup := &sync.WaitGroup{}

	durations := []time.Duration{300, 500, 1000, 100, 50, 760}
	for id, duration := range durations {
		waitGroup.Add(1)
		go complexCalculation(id, duration, waitGroup)
	}

	waitGroup.Wait()
	fmt.Println("All goroutines terminated")

}
```

# Testing

## unit tests

Unit testing in go is done using the `testing` package, which provides methods for logging test information or failing tests.

* Unit tests are stored in files named `*_test.go` such as: `main_test.go`
* The test methods have a single parameter `t *testing.T`
* The test method name must begin with `Test*`, normally followed by the name of the method being tested, like `TestDivide`
* Calls to `t.Error` indicate a failure, the same test continue executing 
* Calls to `t.Fatal` indicate a failure, stop current test method and proceed with the next method
* `t.Log` can be used to output debug information which is only visible when using the `-v` flag

Running tests on the local folder

```
go test
```

With enabled custom `t.Log` output

```
go test -v
```

Running tests on a specific folder

```
go test mongodb/driver
```

Running tests on this and all sub folders

```
go test ./...
```

## code coverage

For displaying the package coverage just append a `-cover` parameter to the `test` call

```
go test -cover
```

Generating a coverage profile file

```
go test -cover -coverprofile=c.out
```

Generating a HTML view of the coverage
```
go tool cover -html=c.out -o coverage.html
```

# Useful Links

* https://golang.org/
* https://github.com/golang/go/wiki
* https://golang.org/doc/code.html
* https://github.com/golang/go/wiki/IDEsAndTextEditorPlugins
* https://tour.golang.org/
* https://golang.org/pkg/
* http://hackthology.com/object-oriented-inheritance-in-go.html
* https://talks.golang.org/2012/10things.slide
* https://blog.alexellis.io/golang-writing-unit-tests/
