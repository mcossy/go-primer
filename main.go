package main

import (
	"fmt"
	"errors"
)

func main() {
	fmt.Println("hi there, welcome to Go!")
}


func Divide(a, b float64) (float64, error) {
	if b == .0 {
		return .0, errors.New("cannot divide by zero")
	}
	return a / b, nil
}
